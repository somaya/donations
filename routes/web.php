<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;


Auth::routes();
Route::get('/', 'website\HomeController@index');
Route::view('/about', 'about');


Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/register-types', 'Auth\RegisterController@types');
Route::get('/needy-register', 'Auth\RegisterController@needyRegister');
Route::post('/needy-register', 'Auth\RegisterController@needyCreate');
Route::get('/donator-register', 'Auth\RegisterController@donatorRegister');
Route::post('/donator-register', 'Auth\RegisterController@donatorCreate');

Route::group(['middleware' => ['web']], function () {
});
Route::group(['middleware' => ['auth']], function () {
    Route::get('/profile', 'website\UsersController@profile');
    Route::post('/donation/store', 'website\HomeController@donationStore');
    Route::get('/donation/request/{id}', 'website\HomeController@donationRequest');
    Route::get('/donation/deliver/{id}', 'website\HomeController@donationDeliver');
    Route::get('/order/deliver/{id}', 'website\HomeController@orderDeliver');
    Route::get('/posts/{id}', 'website\PostsController@getPost');
    Route::get('/home', 'website\HomeController@home');



});



Route::group(['prefix' => 'webadmin', 'middleware' => ['webadmin']], function () {
    Route::get('/dashboard', 'admin\DashboardController@index');
    Route::get('/logout', 'Auth\LoginController@logout');
    Route::resource('/employees', 'admin\EmployeesController');
    Route::resource('/admins', 'admin\AdminsController');
    Route::resource('/donators', 'admin\DonatorsController');
    Route::resource('/donations', 'admin\DonationsController');
    Route::resource('/orders', 'admin\OrdersController');
    Route::get('/donations/{id}/assign/create', 'admin\DonationsController@createAssign');
    Route::post('/donations/{id}/assign/store', 'admin\DonationsController@storeAssign');
    Route::get('/orders/{id}/assign/create', 'admin\OrdersController@createAssign');
    Route::post('/orders/{id}/assign/store', 'admin\OrdersController@storeAssign');
    Route::get('/orders/{id}/approve', 'admin\OrdersController@approve');
    Route::get('/orders/{id}/reject', 'admin\OrdersController@reject');
    Route::resource('/needy', 'admin\NeedyController');
    Route::resource('/categories', 'admin\CategoriesController');
    Route::resource('/posts', 'admin\PostsController');
    Route::get('/get-product-category-data/{product_id}/{category_id}', 'admin\ProductsController@ProductData');
    Route::resource('/sliders', 'admin\SlidersController');
    Route::resource('/user/{id}/rates', 'admin\RatesController');
    Route::get('/send-email', 'admin\UsersController@sendEmail');
    Route::post('/store-email', 'admin\UsersController@storeEmail');



});
