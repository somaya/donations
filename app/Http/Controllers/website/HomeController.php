<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Donation;
use App\Models\donation_image;
use App\Models\Order;
use App\Models\Post;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;


class HomeController extends Controller
{

    public function home()
    {

        $donations = Donation::orderBy('created_at','desc')->get();
        $categories=Category::all();
        $donator_tasks=Donation::where('employee_id',auth()->id())->get();
        $needy_tasks=Order::where('employee_id',auth()->id())->get();
        return view('website.home', compact('donations','categories','donator_tasks','needy_tasks'));
    }
    public function index()
    {

        $donations = Donation::orderBy('created_at','desc')->get();
//        $categories=Category::all();
//        $donator_tasks=Donation::where('employee_id',auth()->id())->get();
//        $needy_tasks=Order::where('employee_id',auth()->id())->get();
        return view('index', compact('donations'));
    }
    public function donationStore(Request $request)
    {
        $request->validate([

            'pickup_date' => 'required',
            'description' => 'required',
            'donation_type' => 'required',

        ]);
        $donation = Donation::create([
            'pickup_date' => $request->pickup_date,
            'description' => $request->description,
            'category_id' => $request->donation_type,
            'user_id' => auth()->id(),
        ]);
        if($request->images){
            foreach ($request->images as $image){
                $imageName = Str::random(10). '.' . $image->extension();
                $image->move(
                    base_path() . '/public/uploads/', $imageName
                );
                donation_image::create([
                    'donation_id'=>$donation->id,
                    'image'=>'/uploads/' . $imageName
                ]);


            }
        }

        return redirect()->back()->with('success','Donation Sent To Admin Successfully');
    }

    public function donationRequest($id)
    {

        $order = Order::create([
            'donation_id' => $id,
            'user_id' => auth()->id(),
        ]);

        return redirect()->back()->with('success','Donation Request Sent To Admin Successfully');
    }
    public function donationDeliver($id)
    {
        $donation=Donation::find($id);
        $donation->update(['status'=>'Delivered']);

        return redirect()->back()->with('success','Mark As Delivered Successfully');
    }
    public function orderDeliver($id)
    {
        $order=Order::find($id);
        $order->update(['status'=>'Delivered']);

        return redirect()->back()->with('success','Mark As Delivered Successfully');
    }



}
