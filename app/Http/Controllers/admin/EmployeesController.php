<?php

namespace App\Http\Controllers\admin;

use App\Models\BankData;
use App\Models\CarOrder;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\MobileData;
use App\Models\PropertyOrder;
use App\Models\Transfer;
use App\Notifications\BookingRequestReceived;
use App\Notifications\FromAdmin;
use App\employee;
use App\User;
use Helpers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Notification;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $employees = User::where('role',2)->get();

        return view('admin.employees.index', compact('employees'));
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.employees.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'f_name' => 'required',
            'l_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);
        $employee = User::create([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'role' => 2,
        ]);
        if ($request->hasFile('photo')) {
            $request->validate([
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10). '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $employee->photo = '/uploads/profiles/' . $imageName;
            $employee->save();

        }


        return redirect('/webadmin/employees')->withFlashMessage(json_encode(['success' => true, 'msg' => 'employee Added Successfully']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = User::where('id', $id)->first();
        return view('admin.employees.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = User::where('id', $id)->first();
        return view('admin.employees.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'f_name' => 'required',
            'l_name' => 'required',
            'phone' => 'required',


        ]);
        $employee = User::where('id', $id)->first();

        if ($request->email != $employee->email) {
            $this->validate($request, [
                'email' => 'email|unique:users',
            ]);
            $employee->update([
                'email' => $request->email
            ]);

        }
        $employee->update([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'phone' => $request->phone,

        ]);


        if ($request->password != '') {
            $employee->update([
                'password' => Hash::make($request->password),
            ]);
        }
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $employee->photo = '/uploads/profiles/' . $imageName;
            $employee->save();

        }


        return redirect('/webadmin/employees' )->withFlashMessage(json_encode(['success' => true, 'msg' => 'employee Edited Successfully']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = User::where('id', $id)->first();
        $employee->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'employee Deleted Successsfully']));
    }



}
