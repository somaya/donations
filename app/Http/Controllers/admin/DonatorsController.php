<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DonatorsController extends Controller
{
    public function index()
    {
        $donators = User::where('role',4)->get();

        return view('admin.donators.index', compact('donators'));
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.donators.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'f_name' => 'required',
            'l_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'address' => 'required',
        ]);
        $donator = User::create([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'password' => Hash::make($request->password),
            'role' => 4,
        ]);
        if ($request->hasFile('photo')) {
            $request->validate([
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10). '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $donator->photo = '/uploads/profiles/' . $imageName;
            $donator->save();

        }


        return redirect('/webadmin/donators')->withFlashMessage(json_encode(['success' => true, 'msg' => 'Donator Added Successfully']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $donator = User::where('id', $id)->first();
        return view('admin.donators.show', compact('donator'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $donator = User::where('id', $id)->first();
        return view('admin.donators.edit', compact('donator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'f_name' => 'required',
            'l_name' => 'required',
            'phone' => 'required',
            'address' => 'required',


        ]);
        $donator = User::where('id', $id)->first();

        if ($request->email != $donator->email) {
            $this->validate($request, [
                'email' => 'email|unique:users',
            ]);
            $donator->update([
                'email' => $request->email
            ]);

        }
        $donator->update([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'phone' => $request->phone,
            'address' => $request->address,

        ]);

        if ($request->password != '') {
            $donator->update([
                'password' => Hash::make($request->password),
            ]);
        }
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $donator->photo = '/uploads/profiles/' . $imageName;
            $donator->save();

        }


        return redirect('/webadmin/donators' )->withFlashMessage(json_encode(['success' => true, 'msg' => 'Donator Edited Successfully']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $donator = User::where('id', $id)->first();
        $donator->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'Donator Deleted Successsfully']));
    }

}
