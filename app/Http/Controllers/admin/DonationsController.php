<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Donation;
use App\User;
use Illuminate\Http\Request;

class DonationsController extends Controller
{
    public function index()
    {
        $donations = Donation::orderBy('created_at', 'ASC')->get();

        return view('admin.donations.index', compact('donations'));
    }

    public function show($id)
    {
        $donation = Donation::where('id', $id)->first();
        return view('admin.donations.show', compact('donation'));
    }
    public function createAssign($id)
    {
        $donation = Donation::where('id', $id)->first();
        $employees=User::where('role',2)->get();
        return view('admin.donations.assign', compact('donation','employees'));
    }
    public function StoreAssign(Request $request,$id)
    {
        $request->validate([
            'employee_id' => 'required',

        ]);
        $donation = Donation::where('id', $id)->first();
        $donation->update(['employee_id'=>$request->employee_id]);
        return redirect('/webadmin/donations')->withFlashMessage(json_encode(['success' => true, 'msg' => 'Assigned To Employee successfully']));
    }

  

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $donation = Donation::where('id', $id)->first();
        $donation->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'Donation Deleted Successsfully']));
    }
}
