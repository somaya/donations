<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\User;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = Order::orderBy('created_at', 'ASC')->get();

        return view('admin.orders.index', compact('orders'));
    }

    public function show($id)
    {
        $order = order::where('id', $id)->first();
        return view('admin.orders.show', compact('order'));
    }
    public function createAssign($id)
    {
        $order = order::where('id', $id)->first();
        $employees=User::where('role',2)->get();
        return view('admin.orders.assign', compact('order','employees'));
    }
    public function StoreAssign(Request $request,$id)
    {
        $request->validate([
            'employee_id' => 'required',

        ]);
        $order = order::where('id', $id)->first();
        $order->update(['employee_id'=>$request->employee_id]);
        return redirect('/webadmin/orders')->withFlashMessage(json_encode(['success' => true, 'msg' => 'Assigned To Employee successfully']));
    }
    public function approve($id)
    {

        $order = order::where('id', $id)->first();
        $order->update(['approve'=>1]);
        return redirect('/webadmin/orders')->withFlashMessage(json_encode(['success' => true, 'msg' => 'Order Approved successfully']));
    }
    public function reject($id)
    {

        $order = order::where('id', $id)->first();
        $order->delete();
        return redirect('/webadmin/orders')->withFlashMessage(json_encode(['success' => true, 'msg' => 'Order Rejected successfully']));
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = order::where('id', $id)->first();
        $order->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'order Deleted Successsfully']));
    }
}
