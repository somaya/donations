<?php

namespace App\Http\Controllers\admin;


use App\Models\Category;
use App\Models\Choice;
use App\Models\Company;
use App\Models\Contact;
use App\Models\ContractRequest;
use App\Models\Donation;
use App\Models\Maintenance;
use App\Models\Meeting;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Post;
use App\Models\PriceRequest;
use App\Models\Product;
use App\Models\RequestService;
use App\Models\Setting;
use App\Models\Startup;
use App\User;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){

        $employees=User::where('role',2)->count();
        $donators=User::where('role',4)->count();
        $needy_people=User::where('role',3)->count();
        $admins=User::where('role',1)->count();
        $orders=Order::count();
        $donations=Donation::count();


        return view('admin.index',compact('employees','donations','admins','orders','needy_people','donators'));
    }
}
