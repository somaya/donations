<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class NeedyController extends Controller
{
    public function index()
    {
        $need = User::where('role',3)->get();

        return view('admin.needy.index', compact('need'));
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.needy.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'f_name' => 'required',
            'l_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'address' => 'required',
            'iban' => 'required',
            'national_id' => 'required',
            'income' => 'required',
            'social_status' => 'required',
        ]);
        $needy = User::create([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'iban' => $request->iban,
            'national_id' => $request->national_id,
            'income' => $request->income,
            'social_status' => $request->social_status,
            'password' => Hash::make($request->password),
            'role' => 3,
        ]);
        if ($request->hasFile('photo')) {
            $request->validate([
                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);

            $imageName = Str::random(10). '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $needy->photo = '/uploads/profiles/' . $imageName;
            $needy->save();

        }


        return redirect('/webadmin/needy')->withFlashMessage(json_encode(['success' => true, 'msg' => 'needy Added Successfully']));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $needy = User::where('id', $id)->first();
        return view('admin.needy.show', compact('needy'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $needy = User::where('id', $id)->first();
        return view('admin.needy.edit', compact('needy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([

            'f_name' => 'required',
            'l_name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'iban' => 'required',
            'national_id' => 'required',
            'income' => 'required',
            'social_status' => 'required',


        ]);
        $needy = User::where('id', $id)->first();

        if ($request->email != $needy->email) {
            $this->validate($request, [
                'email' => 'email|unique:users',
            ]);
            $needy->update([
                'email' => $request->email
            ]);

        }
        $needy->update([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'phone' => $request->phone,
            'address' => $request->address,
            'iban' => $request->iban,
            'national_id' => $request->national_id,
            'social_status' => $request->social_status,
            'income' => $request->income,
        ]);


        if ($request->password != '') {
            $needy->update([
                'password' => Hash::make($request->password),
            ]);
        }
        if ($request->hasFile('photo')) {
            $request->validate([

                'photo' => 'image|mimes:jpeg,png,jpg,gif,svg'

            ]);

            $imageName = Str::random(10) . '.' . $request->file('photo')->extension();
            $request->file('photo')->move(
                base_path() . '/public/uploads/profiles/', $imageName
            );
            $needy->photo = '/uploads/profiles/' . $imageName;
            $needy->save();

        }


        return redirect('/webadmin/needy' )->withFlashMessage(json_encode(['success' => true, 'msg' => 'needy Edited Successfully']));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $needy = User::where('id', $id)->first();
        $needy->delete();
        return redirect()->back()->withFlashMessage(json_encode(['success' => true, 'msg' => 'needy Deleted Successsfully']));
    }
}
