<?php

namespace App;

use App\Models\Category;
use App\Models\Order;
use App\Models\Rate;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'f_name', 'l_name','role','photo','email', 'password',
//    ];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function isRquested($id){
      return  Order::where('user_id',$this->id)->where('donation_id',$id)->exists();
    }
    public function rate(){
        $rates=Rate::where('user_id',$this->id)->get();
        if ($rates->isNotEmpty()){
            $avg=round($rates->sum('degree')/$rates->count(),1);

        }
        else{
            $avg=0;
        }
        return $avg;

    }
}
