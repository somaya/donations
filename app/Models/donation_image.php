<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class donation_image extends Model
{
    protected $table = 'donation_images';
    protected $guarded = [];

    public $timestamps = true;
}
