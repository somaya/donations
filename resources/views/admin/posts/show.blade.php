@extends('admin.layouts.app')

@section('title')
    Post Details
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Dashboard</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/posts')}}" class="m-menu__link">
            <span class="m-menu__link-text">Posts</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">Post Details</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        Post Details
                    </h3>
                </div>
            </div>
        </div>


    <!--begin::Form-->
        {!! Form::model($post,['route' => ['posts.show' , $post->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">



            <div class="form-group m-form__group row">
                <label class="col-lg-1 col-form-label"> Title  </label>
                <div class="col-lg-5{{ $errors->has('title') ? ' has-danger' : '' }}">
                    {!! Form::text('title',old('title'),['class'=>'form-control m-input','autofocus','disabled' ]) !!}

                </div>
                <label class="col-lg-1 col-form-label">Category</label>
                <div class="col-lg-5{{ $errors->has('price') ? ' has-danger' : '' }}">
{{--                    {!! Form::text('price',old('price'),['class'=>'form-control m-input','disabled' ]) !!}--}}
                    <input type="text" class="form-control m-input" value="{{$post->category->name}}" disabled >

                </div>

                <div class="col-lg-12"></div>


                <label class="col-lg-2 col-form-label">Subject</label>
                <div class="col-lg-10{{ $errors->has('body') ? ' has-danger' : '' }}">
                    {!! Form::textarea('body',old('body'),['class'=>'form-control m-input','disabled' ]) !!}
                </div>


            </div>

            @if(isset($post) && $post->image)
                <div class="row">
                    {{--@foreach($post->image as $photo)--}}
                        <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                            <img data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide" alt="First slide [800x4a00]" src="{{$post->image}}" style="height: 150px; width: 150px" data-holder-rendered="true">
                        </div>
                    {{--@endforeach--}}
                </div>
            @endif








        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

