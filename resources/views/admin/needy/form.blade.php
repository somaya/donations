
<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label"> First Name</label>
    <div class="col-lg-3{{ $errors->has('f_name') ? ' has-danger' : '' }}">
        {!! Form::text('f_name',old('f_name'),['class'=>'form-control m-input','autofocus','placeholder'=> 'First Name' ]) !!}
        @if ($errors->has('f_name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('f_name ') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">Last Name</label>
    <div class="col-lg-3{{ $errors->has('l_name') ? ' has-danger' : '' }}">
        {!! Form::text('l_name',old('l_name'),['class'=>'form-control m-input','placeholder'=> 'Last Name' ]) !!}
        @if ($errors->has('l_name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('l_name') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">Phone</label>
    <div class="col-lg-3{{ $errors->has('phone') ? ' has-danger' : '' }}">
        {!! Form::text('phone',old('phone'),['class'=>'form-control m-input','placeholder'=> 'Phone Number' ]) !!}
        @if ($errors->has('phone'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

</div>


<div class="form-group m-form__group row">

    <label class="col-lg-1 col-form-label">Email</label>
    <div class="col-lg-5{{ $errors->has('email') ? ' has-danger' : '' }}">
        {!! Form::text('email',old('email'),['class'=>'form-control m-input','placeholder'=> 'Email' ]) !!}
        @if ($errors->has('email'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">Password</label>
    <div class="col-lg-5{{ $errors->has('password') ? ' has-danger' : '' }}">
        {!! Form::password('password',['class'=>'form-control m-input','placeholder'=> 'Password' ]) !!}
        @if ($errors->has('password'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">Address</label>
    <div class="col-lg-11{{ $errors->has('address') ? ' has-danger' : '' }}">
        {!! Form::text('address',old('address'),['class'=>'form-control m-input','placeholder'=> 'Address' ]) !!}
        @if ($errors->has('address'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">Socail Status</label>
    <div class="col-lg-11{{ $errors->has('address') ? ' has-danger' : '' }}">
{{--        {!! Form::text('address',old('address'),['class'=>'form-control m-input','placeholder'=> 'Address' ]) !!}--}}
        <select name="social_status" class="form-control m-input">
            <option value=""> select social status</option>
            <option value="Single">Single</option>
            <option value="Married">Married</option>


        </select>
        @if ($errors->has('social_status'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('social_status') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label"> IBAN</label>
    <div class="col-lg-3{{ $errors->has('iban') ? ' has-danger' : '' }}">
        {!! Form::text('iban',old('iban'),['class'=>'form-control m-input','autofocus','placeholder'=> 'IBAN' ]) !!}
        @if ($errors->has('iban'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('iban ') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">National ID</label>
    <div class="col-lg-3{{ $errors->has('national_id') ? ' has-danger' : '' }}">
        {!! Form::text('national_id',old('national_id'),['class'=>'form-control m-input','placeholder'=> 'National ID' ]) !!}
        @if ($errors->has('national_id'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('national_id') }}</strong>
            </span>
        @endif
    </div>
    <label class="col-lg-1 col-form-label">Income</label>
    <div class="col-lg-3{{ $errors->has('income') ? ' has-danger' : '' }}">
        {!! Form::text('income',old('income'),['class'=>'form-control m-input','placeholder'=> 'Income' ]) !!}
        @if ($errors->has('income'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('income') }}</strong>
            </span>
        @endif
    </div>

</div>





<div class="form-group m-form__group row">
    <label class="col-lg-1 col-form-label">Photo</label>
    <div class="col-lg-11{{ $errors->has('photo') ? ' has-danger' : '' }}">

        <input type="file" name="photo"   class="form-control uploadinput">
        @if ($errors->has('photo'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

</div>
@if($needy ?? '' && $needy->photo)
<div class="row">




            <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                <img
                        data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                        alt="First slide [800x4a00]"
                        src="{{asset($needy->photo)}}"
                        style="height: 150px; width: 150px"
                        data-holder-rendered="true">
            </div>

</div>
@endif


