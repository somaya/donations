@extends("admin.layouts.app")
@section('title')
    Dashbord
@endsection
@section('content')
    <div class="page-wrapper">

        <div class="content container-fluid">

            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-12">
                        {{--<h3 class="page-title">اهلا!</h3>--}}
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item active">Admin Panel</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Page Header -->

            <div class="row">
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$donators}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/donators"><h6 class="text-muted">Donators</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$donators}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$needy_people}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/needy"><h6 class="text-muted">Needy People</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$needy_people}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$employees}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/employees"><h6 class="text-muted">Employees</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$employees}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-xl-3 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                                <div class="dash-count">
                                    <h3>{{$admins}}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <a href="/webadmin/admins"><h6 class="text-muted">Admins</h6></a>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary w-{{$admins}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
            <div class="col-xl-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                            <div class="dash-count">
                                <h3>{{$orders}}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <a href="/webadmin/orders"><h6 class="text-muted">Orders</h6></a>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary w-{{$orders}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
                            <div class="dash-count">
                                <h3>{{$donations}}</h3>
                            </div>
                        </div>
                        <div class="dash-widget-info">
                            <a href="/webadmin/donations"><h6 class="text-muted">Donations</h6></a>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-primary w-{{$donations}}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>


        </div>
    </div>
@endsection
