@extends('admin.layouts.app')
@section('title')
    Donations
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Dashboard</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/donations')}}" class="m-menu__link">
            <span class="m-menu__link-text">Donations</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Donations
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">



            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result"
                   id="m_table_testArea">

                <thead>
                <tr>
                    <th>#</th>
                    <th>Donation Type</th>
                    <th>Pickup Date</th>
                    <th>Donator Name</th>
                    {{--<th>Donator Phone</th>--}}
                    <th>Created_at</th>
                    <th>Assigned Employee</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($donations as $index=> $donation)
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$donation->category->name}} </td>
                        <td>{{$donation->pickup_date}} </td>
                        <td><a href="/webadmin/donators/{{$donation->user->id}}">{{$donation->user->f_name}} {{$donation->user->l_name}}</a></td>
{{--                        <td>{{$donation->user->phone}} </td>--}}
                        <td>{{$donation->created_at->diffForHumans()}} </td>
                        <td>{{$donation->employee?$donation->employee->f_name .$donation->employee->l_name:''}} </td>
                        <td>{{$donation->employee?$donation->status:''}} </td>
                        <td>

                            @if(!$donation->employee)
                            <a title="Assign To Employee" href="/webadmin/donations/{{$donation->id}}/assign/create"><i class="fa fa-user-alt"></i></a>
                            @endif
                            <a title="Show" href="/webadmin/donations/{{$donation->id}}"><i class="fa fa-eye"></i></a>

                            <form class="inline-form-style"
                                  action="/webadmin/donations/{{ $donation->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}

@endsection
