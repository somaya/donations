<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">Dashboard</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/employees')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">Employees</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/admins')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-users-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">Admins</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/donators')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user-ok"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">Donators</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/needy')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-users"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">Needy People</span>
            </span>
        </span>
    </a>
</li>

<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/categories')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-technology-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">Donation Types </span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/donations')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">Donations</span>
            </span>
        </span>
    </a>
</li>
<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('/webadmin/orders')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-cart"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">Orders</span>
            </span>
        </span>
    </a>
</li>
{{--<li class="m-menu__item" aria-haspopup="true">--}}
    {{--<a href="{{url('/webadmin/send-email')}}" class="m-menu__link ">--}}
        {{--<i class="m-menu__link-icon flaticon-file"></i>--}}
        {{--<span class="m-menu__link-title">--}}
            {{--<span class="m-menu__link-wrap">--}}
                {{--<span class="m-menu__link-text">Send Emails</span>--}}
            {{--</span>--}}
        {{--</span>--}}
    {{--</a>--}}
{{--</li>--}}


