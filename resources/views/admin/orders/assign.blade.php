@extends('admin.layouts.app')

@section('title')
    Assign To Employee
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Dashboard</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/orders')}}" class="m-menu__link">
            <span class="m-menu__link-text">  Orders </span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">Assign To Employee </span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        Assign To Employee
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right" action="/webadmin/orders/{{$order->id}}/assign/store" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="m-portlet__body">

                <div class="form-group m-form__group row">
                    <label class="col-lg-2 col-form-label">Employee : </label>
                    <div class="col-lg-10{{ $errors->has('name') ? ' has-danger' : '' }}">
                        {{--{!! Form::text('name',null,['class'=>'form-control m-input','autofocus','placeholder'=>"Name"]) !!}--}}
                        <select name="employee_id" class="form-control m-input">
                            <option value="">Select Employee</option>
                            @foreach($employees as $employee)
                                <option value="{{$employee->id}}">{{$employee->f_name}} {{$employee->l_name}}</option>
                            @endforeach

                        </select>
                        @if ($errors->has('employee_id'))
                            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('employee_id') }}</strong>
            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-success">Assign </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection


