@extends('admin.layouts.app')

@section('title')
    Order Details
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Dashboard</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/orders')}}" class="m-menu__link">
            <span class="m-menu__link-text">Orders</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="" class="m-menu__link">
            <span class="m-menu__link-text">Order Details </span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        Order Details
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        {!! Form::model($order,['route' => ['orders.show' , $order->id],'method'=> 'get','class'=>'m-form m-form--fit m-form--label-align-right',"enctype"=>"multipart/form-data"]) !!}
        <div class="m-portlet__body">


            <div class="form-group m-form__group row">

                <label class="col-lg-1 col-form-label">Donation Type</label>
                <div class="col-lg-5{{ $errors->has('order_type') ? ' has-danger' : '' }}">
                    <input type="text" disabled class="form-control m-input" value="{{$order->donation->category->name}}">

                </div>
                <label class="col-lg-1 col-form-label">Donator</label>
                <div class="col-lg-5{{ $errors->has('phone') ? ' has-danger' : '' }}">
                    <input type="text" disabled class="form-control m-input" value="{{$order->donation->user->f_name}} {{$order->donation->user->l_name}}">

                </div>


            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">Needy Name</label>
                <div class="col-lg-10{{ $errors->has('address') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->user->f_name}} {{$order->user->l_name}}" disabled="">

                </div>

            </div>


            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">Needy Phone</label>
                <div class="col-lg-10{{ $errors->has('email') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->user->phone}}" disabled="">

                </div>


            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">Needy Address</label>
                <div class="col-lg-10{{ $errors->has('address') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->user->address}}" disabled="">

                </div>

            </div>
            @if($order->employee)
            <div class="form-group m-form__group row">

                <label class="col-lg-2 col-form-label">Assigned Employee</label>
                <div class="col-lg-5{{ $errors->has('email') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->employee->f_name}} {{$order->employee->l_name}}" disabled="">

                </div>


            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-2 col-form-label">Deliver Status</label>
                <div class="col-lg-5{{ $errors->has('address') ? ' has-danger' : '' }}">
                    <input type="text" class="form-control m-input" value="{{$order->status}}" disabled="">

                </div>

            </div>
            @endif


            @if(isset($order) && $order->donation->images)
                <div class="row">
                    @foreach($order->donation->images as $image)


                    <div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 10px;">

                        <img
                            data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide"
                            alt="First slide [800x4a00]"
                            src="{{asset($image->image)}}"
                            style="height: 150px; width: 150px"
                            data-holder-rendered="true">
                    </div>
                    @endforeach

                </div>
            @endif



        </div>

    {!! Form::close() !!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

