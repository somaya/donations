@extends('admin.layouts.app')
@section('title')
    orders
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Dashboard</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/orders')}}" class="m-menu__link">
            <span class="m-menu__link-text">orders</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        orders
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">



            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result"
                   id="m_table_testArea">

                <thead>
                <tr>
                    <th>#</th>
                    <th>Donation Id</th>
                    <th>Needy Name</th>
                    <th>Created_at</th>
                    <th>Approve</th>
                    <th>Assigned Employee</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $index=> $order)
                    <tr>
                        <td>{{++$index}}</td>
                        <td><a href="/webadmin/donations/{{$order->donation->id}}">{{$order->donation->id}} </a></td>
                        <td><a href="/webadmin/needy/{{$order->user->id}}">{{$order->user->f_name}} {{$order->user->l_name}}</a></td>
                        <td>{{$order->created_at->diffForHumans()}} </td>
                        <td>{{$order->approve==1?'Approved':'Pending'}} </td>
                        <td>{{$order->employee?$order->employee->f_name .$order->employee->l_name:''}} </td>
                        <td>{{$order->employee?$order->status:''}} </td>
                        <td>
                            <a title="Show" href="/webadmin/orders/{{$order->id}}"><i class="fa fa-eye"></i></a>


                        @if(!$order->employee && $order->approve==1)
                            <a title="Assign To Employee" href="/webadmin/orders/{{$order->id}}/assign/create"><i class="fa fa-user-alt"></i></a>
                            @endif
                            @if( $order->approve==0)
                                <a title="Approve" href="/webadmin/orders/{{$order->id}}/approve"><i class="fa fa-check-circle"></i></a>
                                <a title="Reject" href="/webadmin/orders/{{$order->id}}/reject"><i class="fa fa-ban"></i></a>
                            @endif

                            <form class="inline-form-style"
                                  action="/webadmin/orders/{{ $order->id }}"
                                  method="post">
                                <button type="submit" class="trash-btn">
                                    <span class="fa fa-trash"></span>
                                </button>
                                <input type="hidden" name="_method" value="delete"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </form>

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}

@endsection
