@extends('admin.layouts.app')
@section('title')
    Reviews
@endsection

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('/webadmin/dashboard')}}" class="m-menu__link">
            <span class="m-menu__link-text">Dashboard</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('/webadmin/user'.$user->id.'/rates')}}" class="m-menu__link">
            <span class="m-menu__link-text">Reviews</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>


@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Reviews
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div><a href="/webadmin/user/{{$user->id}}/rates/create" style="margin-bottom:20px"
                    class="btn btn_primary btn btn-danger"><i class=" fa fa-edit"></i>Add Review</a></div>
            <br>


            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable search_result"
                   >

                <thead>
                <tr>
                    <th>#</th>
                    <th>Reviewed By</th>
                    <th>Review</th>
                    <th>Comment</th>
                    <th>Time</th>
                    {{--<th>الادوات</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($rates as $index=> $rate)
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$rate->admin->f_name}} {{$rate->admin->l_name}} </td>
                        <td>{{$rate->degree}} </td>
                        <td>{{$rate->comment}} </td>
                        <td>{{$rate->created_at->isoFormat('MMMM Do YYYY, h:mm:ss ')}} </td>
                        <td>


{{--                            <a title="عرض" href="/webadmin/user/{{$user->id}}/rates/{{$rate->id}}"><i class="fa fa-eye"></i></a>--}}
                            {{--<a title="تعديل" href="/webadmin/users/{{$user->id}}/edit"><i class="fa fa-edit"></i></a>--}}
                            {{--<form class="inline-form-style"--}}
                                  {{--action="/webadmin/users/{{ $user->id }}"--}}
                                  {{--method="post">--}}
                                {{--<button type="submit" class="trash-btn">--}}
                                    {{--<span class="fa fa-trash"></span>--}}
                                {{--</button>--}}
                                {{--<input type="hidden" name="_method" value="delete"/>--}}
                                {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                            {{--</form>--}}

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {{--{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}--}}
    {{--{!! Html::script('admin/custom/js/testArea/script.js') !!}--}}

@endsection
