<div class="form-group m-form__group row">
    <label for="main_category" class="col-lg-2 col-form-label">التقييم : </label>
    <div class="col-lg-10{{ $errors->has('degree') ? ' has-danger' : '' }}">
        <select name="degree"   class="form-control m-input" required>
            <option value="">Choose degree from 5 </option>
                <option value="1" {{isset($rate) && $rate->degree==1 ?'selected':''}}>1</option>
                <option value="2" {{isset($rate) && $rate->degree==2 ?'selected':''}}>2</option>
                <option value="3" {{isset($rate) && $rate->degree==3 ?'selected':''}}>3</option>
                <option value="4" {{isset($rate) && $rate->degree==4 ?'selected':''}}>4</option>
                <option value="5" {{isset($rate) && $rate->degree==5 ?'selected':''}}>5</option>

        </select>
        @if ($errors->has('degree'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('degree') }}</strong>
            </span>
        @endif
    </div>

</div>
<div class="form-group m-form__group row">
    <label class="col-lg-2 col-form-label">Comment  : </label>
    <div class="col-lg-10{{ $errors->has('comment') ? ' has-danger' : '' }}">
        {!! Form::textarea('comment',null,['class'=>'form-control m-input','autofocus','required','placeholder'=>"التعليق"]) !!}
        @if ($errors->has('comment'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('comment') }}</strong>
            </span>
        @endif
    </div>

</div>



