@extends("website.layouts.app")
@section('content')
    <div class="container">
        <div class="main-body">

            <!-- Breadcrumb -->
            <nav aria-label="breadcrumb" class="main-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/posts">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page"> Profile</li>
                </ol>
            </nav>
            <!-- /Breadcrumb -->

            <div class="row gutters-sm">
                <div class="col-md-4 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-column align-items-center text-center">
                                <img src="{{$user->photo?:'https://bootdey.com/img/Content/avatar/avatar7.png'}}" alt="Admin" class="rounded-circle" width="150">
                                <div class="mt-3">
                                    <h4>{{$user->f_name}} {{$user->l_name}}</h4>
                                    {{--<p class="text-secondary mb-1">اخر ظهور قبل 7 دقيقة</p>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card mt-3">

                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-5 logo-link">
                                    <h5 class="mb-0">Email :</h5>
                                </div>
                                <div class="col-sm-7 text-secondary">
                                    {{$user->email}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-5 logo-link">
                                    <h5 class="mb-0">Phone : </h5>
                                </div>
                                <div class="col-sm-7 text-secondary">
                                    {{$user->phone}}
                                </div>
                            </div>
                            <hr>
                            @if($user->address)
                            <div class="row">
                                <div class="col-sm-5 logo-link">
                                    <h5 class="mb-0">Address :</h5>
                                </div>
                                <div class="col-sm-7 text-secondary">
                                    {{$user->address}}
                                </div>
                            </div>
                            <hr>
                            @endif
                            @if($user->iban)
                                <div class="row">
                                    <div class="col-sm-5 logo-link">
                                        <h5 class="mb-0">IBAN :</h5>
                                    </div>
                                    <div class="col-sm-7 text-secondary">
                                        {{$user->iban}}
                                    </div>
                                </div>
                                <hr>
                            @endif
                            @if($user->national_id)
                                <div class="row">
                                    <div class="col-sm-5 logo-link">
                                        <h5 class="mb-0">National ID :</h5>
                                    </div>
                                    <div class="col-sm-7 text-secondary">
                                        {{$user->national_id}}
                                    </div>
                                </div>
                                <hr>
                            @endif
                            @if($user->income)
                                <div class="row">
                                    <div class="col-sm-5 logo-link">
                                        <h5 class="mb-0">Income :</h5>
                                    </div>
                                    <div class="col-sm-7 text-secondary">
                                        {{$user->income}}
                                    </div>
                                </div>
                                <hr>
                            @endif
                            @if($user->social_status)
                                <div class="row">
                                    <div class="col-sm-5 logo-link">
                                        <h5 class="mb-0">Social Status :</h5>
                                    </div>
                                    <div class="col-sm-7 text-secondary">
                                        {{$user->social_status}}
                                    </div>
                                </div>
                                <hr>
                            @endif


                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection