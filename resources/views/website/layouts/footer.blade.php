
<footer class="hide-in-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="about-veno">
                    <div class="logo">
                        <img src="/website/img/logo.jpeg" alt="Venue Logo" width="140px">
                    </div>
                    <ul class="social-icons">
                        <li>
                            <a href=""><i class="fa fa-facebook"></i></a>
                            <a href=""><i class="fa fa-twitter"></i></a>
                            <a href=""><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            {{--<div class="col-md-3">--}}
                {{--<div class="useful-links">--}}
                    {{--<div class="footer-heading">--}}
                        {{--<h4>معلومات الشركه</h4>--}}
                    {{--</div>--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<ul>--}}
                                {{--<li><a href="#"><i class="fa fa-stop"></i>من نحن</a></li>--}}
                                {{--<li><a href="#"><i class="fa fa-stop"></i>للاعلان على فرصة</a></li>--}}
                                {{--<li><a href="#"><i class="fa fa-stop"></i>شروط الاستخدام</a></li>--}}
                                {{--<li><a href="#"><i class="fa fa-stop"></i>سياسة الخصوصية?</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-3">--}}
                {{--<div class="contact-info">--}}
                    {{--<div class="footer-heading">--}}
                        {{--<h4>بيانات الاتصال</h4>--}}
                    {{--</div>--}}
                    {{--<ul>--}}
                        {{--<li><span>رقم الجوال :</span><a href="{{$setting->phone}}">{{$setting->phone}}</a></li>--}}
                        {{--<li><span>الايميل :</span><a href="{{$setting->email}}">{{$setting->email}}</a></li>--}}
                        {{--<li><span>العنوان :</span><a href="#">{{$setting->address}}</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</footer>




