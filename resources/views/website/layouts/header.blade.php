<div class="overlay-citys"></div>
        <link rel="stylesheet" href="{{asset('/website/css/nav.css')}}">
<style>

</style>
<div class="wrap">

    <div class="upper-bar">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-4 align-self-center">
                    <a href="/home" class="logo-link"><img src="/website/img/logo.jpeg" width="120px"/></a>


                </div>
                <div class="col-md-8 align-self-center hide-in-sm" dir="ltr">

                    @if(!auth()->check())
                        <a href="/login" class="login-header"><span>Login</span> </a>
                    @else
                        <div style="display: inline-flex;" class="login-header">
                            <span data-toggle="dropdown">
                                    <h4>{{auth()->user()->f_name}} {{auth()->user()->l_name}}  <i class="fa fa-caret-down" aria-hidden="true"></i></h4>
                            </span>
                            <ul class="dropdown-menu" style="right: unset">
                                <li><a href="/profile" data-original-title="" title=""> Profile<i class="fa fa-user" aria-hidden="true"></i> </a></li>

                                @if(auth()->user()->role == 1)
                                    <li><a href="/webadmin/dashboard" data-original-title="" title="">Admin Panel <i class="fa fa-users" aria-hidden="true"></i> </a></li>
                                @endif
                                <li><a href="/logout" data-original-title="" title=""> Logout <i class="fa fa-power-off" aria-hidden="true"></i> </a></li>
                            </ul>
                        </div>
                    @endif




                </div>
            </div>
        </div>
    </div>



    <nav  class="navbar navbar-default">

        <ul class="dropdown menu ">
            <li class="col-lg-4"><a href="/">Home</a></li>
            <li class="col-lg-4"><a href="/about">About</a></li>

            <li class="col-lg-4"><a href="/home">
                    @if(auth()->check() && auth()->user()->role==3)
                        Donation Box
                    @elseif(auth()->check() && auth()->user()->role==2)
                        My Tasks
                    @elseif(auth()->check() && auth()->user()->role==4)
                        Donation Form
                    @else
                        Donation Box
                    @endif

                </a></li>


        </ul>
    </nav>





</div>
