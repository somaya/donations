@extends("website.layouts.app")
@section('content')
    <style>
        body{
            margin-top:20px;
            background:#eee;
        }
        .blog-page .single_post {
            -webkit-transition: all .4s ease;
            transition: all .4s ease
        }

        .blog-page .single_post .img-post {
            position: relative;
            overflow: hidden;
            max-height: 500px
        }

        .blog-page .single_post .img-post>img {
            -webkit-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1);
            opacity: 1;
            -webkit-transition: -webkit-transform .4s ease, opacity .4s ease;
            transition: transform .4s ease, opacity .4s ease;
            max-width: 100%;
            filter: none;
            -webkit-filter: grayscale(0);
            -webkit-transform: scale(1.01)
        }

        .blog-page .single_post .img-post:hover img {
            -webkit-transform: scale(1.02);
            -ms-transform: scale(1.02);
            transform: scale(1.02);
            opacity: .7;
            filter: gray;
            -webkit-filter: grayscale(1);
            -webkit-transition: all .8s ease-in-out
        }

        .blog-page .single_post .img-post:hover .social_share {
            display: block
        }

        .blog-page .single_post .img-post .social_share {
            position: absolute;
            bottom: 10px;
            left: 10px;
            display: none
        }

        .blog-page .single_post .meta {
            list-style: none;
            padding: 0;
            margin: 0
        }

        .blog-page .single_post .meta li {
            display: inline-block;
            margin-right: 15px
        }

        .blog-page .single_post .meta li a {
            font-style: italic;
            color: #959595;
            text-decoration: none;
            font-size: 12px
        }

        .blog-page .single_post .meta li a i {
            margin-right: 6px;
            font-size: 12px
        }

        .blog-page .single_post h3 {
            font-size: 20px;
            line-height: 26px;
            -webkit-transition: color .4s ease;
            transition: color .4s ease
        }

        .blog-page .single_post h3 a {
            color: #242424;
            text-decoration: none
        }

        .blog-page .single_post p {
            font-size: 15px
        }

        .blog-page .single_post .blockquote p {
            margin-top: 0 !important
        }

        .blog-page .right-box .categories-clouds li {
            display: inline-block;
            margin-bottom: 5px
        }

        .blog-page .right-box .categories-clouds li a {
            display: block;
            font-size: 14px;
            border: 1px solid #ccc;
            padding: 6px 10px;
            border-radius: 3px;
            color: #242424
        }

        .blog-page .right-box .instagram-plugin {
            overflow: hidden
        }

        .blog-page .right-box .instagram-plugin li {
            float: left;
            overflow: hidden;
            border: 1px solid #fff
        }

        .blog-page .comment-reply li {
            margin-bottom: 15px
        }

        .blog-page .comment-reply li:last-child {
            margin-bottom: none
        }

        .blog-page .comment-reply li h5 {
            font-size: 18px
        }

        .blog-page .comment-reply li p {
            margin-bottom: 0px;
            font-size: 15px;
            color: #777
        }

        .blog-page .comment-reply .list-inline li {
            display: inline-block;
            margin: 0;
            padding-right: 20px
        }

        .blog-page .comment-reply .list-inline li a {
            font-size: 13px
        }

        .page.with-sidebar.right .left-box {
            margin-right: -20px
        }

        @media (max-width: 414px) {
            .section.blog-page {
                padding: 20px 0
            }
            .blog-page .left-box .single-comment-box>ul>li {
                padding: 25px 0
            }
            .blog-page .left-box .single-comment-box ul li .icon-box {
                display: inline-block
            }
            .blog-page .left-box .single-comment-box ul li .text-box {
                display: block;
                padding-left: 0;
                margin-top: 10px
            }
        }
        .card {
            background: #fff;
            margin-bottom: 30px;
            transition: .5s;
            border: 0;
            border-radius: .55rem;
            position: relative;
            width: 100%;
            box-shadow: 0 1px 2px 0 rgba(0,0,0,0.1);
        }

        .card .body {
            font-size: 14px;
            color: #424242;
            padding: 20px;
            font-weight: 400;
        }
        .card .header {
            color: #424242;
            padding: 20px;
            position: relative;
            box-shadow: none;
        }
        .card .header h2 {
            font-size: 15px;
            color: #757575;
            position: relative;
        }
        .card .header h2:before {
            background: #a27ce6;
        }
        .card .header h2::before {
            position: absolute;
            width: 20px;
            height: 1px;
            left: 0;
            top: -20px;
            content: '';
        }
        .m-b-15 {
            margin-bottom: 15px;
        }
    </style>
    {{--if auth user is needy show donations--}}

    @if(auth()->user()->role==3)

        <section class="popular-places" id="popular">
            <div class="container">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
                <div class="container blog-page">
                    <div class="row clearfix">

                        @if($donations->isNotEmpty())
                            <div class="header">
                                <h4><strong>Latest Donations</strong></h4>
                            </div>
                            @foreach($donations as $donation)
                                <div class="col-lg-4 col-md-12">
                                    <div class="card single_post">

                                        <div class="body">
                                            {{--<h3 class="m-t-0 m-b-5"><a href="/donations/{{$donation->id}}">{{$donation->title}}</a></h3>--}}
                                            <ul class="meta">
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-account col-blue"></i>Donated By: {{$donation->user->f_name}} {{$donation->user->l_name}}</a></li>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-label col-amber"></i> {{$donation->category->name}}</a></li>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-time col-blue"></i> {{$donation->created_at->isoFormat('MMMM Do YYYY ')}}</a></li>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-time col-blue"></i>Pick up date: {{$donation->pickup_date}}</a></li>
                                            </ul>
                                        </div>
                                        <div class="body">
                                            <div class="img-post m-b-15">
                                                @if($donation->images->isNotEmpty())
                                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                                        <!-- Indicators -->
                                                        <ol class="carousel-indicators">
                                                            @foreach($donation->images as $index =>$image)

                                                                <li data-target="#myCarousel" data-slide-to="{{$index}}" class="{{$index==0?'active':''}}"></li>
                                                            @endforeach

                                                        </ol>

                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            @foreach($donation->images as $index =>$image)
                                                                <div class="item {{$index==0?'active':''}}">
                                                                    <img src="{{$image->image}}" width="500px" height="300px" alt="Los Angeles">
                                                                </div>
                                                            @endforeach

                                                        </div>

                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                @endif


                                            </div>
                                            <p>{{$donation->description}}</p>
                                            @if(!auth()->user()->isRquested($donation->id))
                                                <a href="/donation/request/{{$donation->id}}" title="Request" class="btn btn-round btn-success">Request</a>
                                            @else
                                                <label  class=" btn-round btn-info">Requested</label>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <h4 style="color: red">No Donations Yet</h4>

                        @endif

                    </div>
                </div>
            </div>
        </section>
    @endif
    {{--end donations --}}
    {{--if auth user is donator--}}
    @if(auth()->user()->role==4)
        <div class="login-page">
            <div class="container">
                <div class="col-md-8 col-md-offset-2 pull-left col-sm-12 col-xs-12">

                    <h3>Donation Form</h3>



                    <div class="login-form">
                        {{--                        @include('message')--}}
                        <form action="/donation/store" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-g row">
                                <label class="col-lg-2 logo-link">Pickup Date</label>
                                <div class="col-lg-10">
                                    <input type="date" name="pickup_date" class="form-control" placeholder="pickup date">
                                    @if ($errors->has('pickup_date'))
                                        <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('pickup_date') }}
                                   </strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-g row">
                                <label class="col-lg-2 logo-link">Description</label>
                                <div class="col-lg-10">
                                    <input type="text"  name="description" class="form-control" placeholder="description">
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('description') }}
                                   </strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-g row">

                                <label class="col-lg-2 logo-link">Donation Type</label>
                                <div class="col-lg-10">
                                    <select name="donation_type" id="donation_type" class="form-control" placeholder="donation type">
                                        <option value="">donation type</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach


                                    </select>
                                    @if ($errors->has('donation_type'))
                                        <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('donation_type') }}
                                   </strong>
                                </span>
                                    @endif
                                </div>
                            </div><br>
                            <div class="form-g row">

                                <label class="col-lg-2 logo-link">Images</label>
                                <div class="col-lg-10">
                                    <input type="file"  name="images[]" class="form-control m-input" multiple class="form-control uploadinput">
                                    @if ($errors->has('images.*'))
                                        <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('images.*') }}
                                   </strong>
                                </span>
                                    @endif
                                </div>
                            </div>


                            {{--<div class="form-g">--}}
                            {{--<input type="checkbox">--}}
                            {{--<span>تذكرني</span>--}}
                            {{--</div>--}}

                            <div class="form-g f-btn">
                                <button type="submit" class="logo-link">Submit</button>
                            </div>
                        </form>



                    </div>
                </div>
            </div>
        </div>
    @endif
    {{--end donation form--}}


    {{--if user is employee show assigned tasks--}}

    @if(auth()->user()->role==2)

        <section class="popular-places" id="popular">
            <div class="container">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
                <div class="container blog-page">
                    <div class="row clearfix">

                        @if($donator_tasks->isNotEmpty())
                            <div class="header">
                                <h4><strong>Assigned Donations To get From Donators</strong></h4>
                            </div>
                            @foreach($donator_tasks as $donation)
                                <div class="col-lg-4 col-md-12">
                                    <div class="card single_post">

                                        <div class="body">
                                            {{--<h3 class="m-t-0 m-b-5"><a href="/donations/{{$donation->id}}">{{$donation->title}}</a></h3>--}}
                                            <ul class="meta">
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-account col-blue"></i><label style="color: #ca4949">Donator:</label> {{$donation->user->f_name}} {{$donation->user->l_name}}</a></li><br>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-label col-amber"></i><label style="color: #ca4949">Donation Type:</label> {{$donation->category->name}}</a></li><br>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-time col-blue"></i><label style="color: #ca4949">Pick up date:</label> {{$donation->pickup_date}}</a></li><br>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-label"></i><label style="color: #ca4949">Donator Address:</label> {{$donation->user->address}}</a></li><br>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-phone"></i><label style="color: #ca4949">Donator Phone:</label> {{$donation->user->phone}}</a></li><br>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-label"></i><label style="color: #ca4949">Donation description:</label> {{$donation->description}}</a></li><br>
                                            </ul>
                                        </div>
                                        <div class="body">
                                            <div class="img-post m-b-15">
                                                @if($donation->images->isNotEmpty())
                                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                                        <!-- Indicators -->
                                                        <ol class="carousel-indicators">
                                                            @foreach($donation->images as $index =>$image)

                                                                <li data-target="#myCarousel" data-slide-to="{{$index}}" class="{{$index==0?'active':''}}"></li>
                                                            @endforeach

                                                        </ol>

                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            @foreach($donation->images as $index =>$image)
                                                                <div class="item {{$index==0?'active':''}}">
                                                                    <img src="{{$image->image}}" width="500px" height="300px" alt="Los Angeles">
                                                                </div>
                                                            @endforeach

                                                        </div>

                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                @endif


                                            </div>
                                            @if($donation->status=='Pending')
                                                <a href="/donation/deliver/{{$donation->id}}" title="Mark As Delivered" class="btn btn-round btn-success">Mark As Delivered</a>
                                            @else
                                                <label  class=" btn-round btn-info">Delivered</label>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <h4 style="color: red">No Assigned Donations From Donator Yet</h4>

                        @endif

                    </div>
                </div>
            </div>
        </section>
        <section class="popular-places" id="popular">
            <div class="container">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
                <div class="container blog-page">
                    <div class="row clearfix">

                        @if($needy_tasks->isNotEmpty())
                            <div class="header">
                                <h4><strong>Assigned Donations To Deliver To Needy People</strong></h4>
                            </div>
                            @foreach($needy_tasks as $donation)
                                <div class="col-lg-4 col-md-12">
                                    <div class="card single_post">

                                        <div class="body">
                                            {{--<h3 class="m-t-0 m-b-5"><a href="/donations/{{$donation->id}}">{{$donation->title}}</a></h3>--}}
                                            <ul class="meta">
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-account col-blue"></i><label style="color: #ca4949">Needy :</label> {{$donation->user->f_name}} {{$donation->user->l_name}}</a></li><br>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-label col-amber"></i><label style="color: #ca4949">Donation Id:</label> {{$donation->donation->id}}</a></li><br>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-label col-amber"></i><label style="color: #ca4949">Donator:</label> {{$donation->donation->user->f_name}} {{$donation->donation->user->l_name}}</a></li><br>
                                                {{--<li><a href="javascript:void(0);"><i class="zmdi zmdi-time col-blue"></i>Pick up date: {{$donation->pickup_date}}</a></li><br>--}}
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-label"></i><label style="color: #ca4949">Needy Address:</label> {{$donation->user->address}}</a></li><br>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-phone"></i><label style="color: #ca4949">Needy Phone:</label> {{$donation->user->phone}}</a></li><br>
                                                <li><a href="javascript:void(0);"><i class="zmdi zmdi-label"></i><label style="color: #ca4949">Donation description: </label>{{$donation->donation->description}}</a></li><br>
                                            </ul>
                                        </div>
                                        <div class="body">
                                            <div class="img-post m-b-15">
                                                @if($donation->donation->images->isNotEmpty())
                                                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                                        <!-- Indicators -->
                                                        <ol class="carousel-indicators">
                                                            @foreach($donation->donation->images as $index =>$image)

                                                                <li data-target="#myCarousel" data-slide-to="{{$index}}" class="{{$index==0?'active':''}}"></li>
                                                            @endforeach

                                                        </ol>

                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner">
                                                            @foreach($donation->donation->images as $index =>$image)
                                                                <div class="item {{$index==0?'active':''}}">
                                                                    <img src="{{$image->image}}" width="500px" height="300px" alt="Los Angeles">
                                                                </div>
                                                            @endforeach

                                                        </div>

                                                        <!-- Left and right controls -->
                                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>
                                                    </div>
                                                @endif


                                            </div>
                                            @if($donation->status=='Pending')
                                                <a href="/order/deliver/{{$donation->id}}" title="Mark As Delivered" class="btn btn-round btn-success">Mark As Delivered</a>
                                            @else
                                                <label  class=" btn-round btn-info">Delivered</label>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <h4 style="color: red">No Assigned Donations To Needy Yet</h4>

                        @endif

                    </div>
                </div>
            </div>
        </section>
    @endif
    {{--end tasks --}}


@endsection
