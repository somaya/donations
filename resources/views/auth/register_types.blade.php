@extends('website.layouts.app')

@section('content')

    <div class="login-page">
        <div class="container">
            <div class="col-md-8 col-md-offset-2 pull-left col-sm-12 col-xs-12">

                <h3>Register As</h3>
                <div class="col-md-4 align-self-center">
                    <img src="/website/img/logo.jpeg" width="120px"/>


                </div>
                <br>


                <div >
                    <div class="row">
                        <div class="col-xl-6 col-sm-6 col-12 login-form">
                            <div class="card">
                                <div class="card-body">
                                    <div class="dash-widget-info">
                                        <a href="/donator-register"><h4 class="text-muted">Donator</h4></a>

                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col-xl-6 col-sm-6 col-12 login-form">
                            <div class="card">
                                <div class="card-body">

                                    <div class="dash-widget-info">
                                        <a href="/needy-register"><h4 class="text-muted " >Needy People</h4></a>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>



@endsection
