@extends("website.layouts.app")
@section('content')
    <div class="login-page">
        <div class="container">
            <div class="col-md-6">

                <h3>Register As Donator</h3>

                <div class="login-form">
                    <form action="/donator-register" method="post">
                        @csrf

                        <div class="form-g">
                            <input type="text" name="f_name" value="{{old('f_name')}}" placeholder="First Name">
                            @if ($errors->has('f_name'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('f_name') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="l_name" value="{{old('l_name')}}" placeholder="Last Name">
                            @if ($errors->has('l_name'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('l_name') }}
                                   </strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-g">
                            <input type="email" name="email" value="{{old('email')}}" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('email') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="password" name="password"  placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="password" name="password_confirmation"  placeholder="Confirm Password">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password_confirmation') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="phone" value="{{old('phone')}}" placeholder="Phone">
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('phone') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="address" value="{{old('address')}}" placeholder="Address">
                            @if ($errors->has('address'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('address') }}
                                   </strong>
                                </span>
                            @endif
                        </div>



                        <div class="form-g f-btn ">
                            <button type="submit" >Register</button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>

@endsection


