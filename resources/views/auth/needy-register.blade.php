@extends("website.layouts.app")
@section('content')
    <div class="login-page">
        <div class="container">
            <div class="col-md-6">

                <h3>Register As Needy </h3>

                <div class="login-form">
                    <form action="/needy-register" method="post">
                        @csrf

                        <div class="form-g">
                            <input type="text" name="f_name" value="{{old('f_name')}}" placeholder="First Name">
                            @if ($errors->has('f_name'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('f_name') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="l_name" value="{{old('l_name')}}" placeholder="Last Name">
                            @if ($errors->has('l_name'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('l_name') }}
                                   </strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-g">
                            <input type="email" name="email" value="{{old('email')}}" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('email') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="password" name="password"  placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="password" name="password_confirmation"  placeholder="Confirm Password">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('password_confirmation') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="phone" value="{{old('phone')}}" placeholder="Phone">
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('phone') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="address" value="{{old('address')}}" placeholder="Address">
                            @if ($errors->has('address'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('address') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="iban" value="{{old('iban')}}" placeholder="IBAN">
                            @if ($errors->has('iban'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('iban') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="national_id" value="{{old('national_id')}}" placeholder="National ID">
                            @if ($errors->has('national_id'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('national_id') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <input type="text" name="income" value="{{old('income')}}" placeholder="Income">
                            @if ($errors->has('income'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('income') }}
                                   </strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-g">
                            <select name="social_status" id="social_status" class="form-control" placeholder="social status">
                                <option value="">social status</option>
                                    <option value="Single">Single</option>
                                    <option value="Married">Married</option>



                            </select>
                            @if ($errors->has('social_status'))
                                <span class="help-block">
                                   <strong style="color: red;">
                                       {{ $errors->first('social_status') }}
                                   </strong>
                                </span>
                            @endif
                        </div><br>


                        <div class="form-g f-btn">
                            <button type="submit" >Register</button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>

@endsection


