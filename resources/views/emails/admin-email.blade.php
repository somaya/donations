
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog</title>
</head>
<body style="background:#f1f1f1">

<div style="background:#292829;padding:20px 20px;text-align:center;">
    <img src="https://renzo.app/website/images/renzo_logo.png" alt="">
</div>
<div style="background:#fff;border:0px;padding:20px 50px;">
    <h1>From Admin </h1>
    <p style="font-size:20px;">{{$msg}}</p>

</div>
<div style="background:#292829;padding:20px 20px;text-align:center;">
    <p style="font-size:10px;color:#fff">
        @ 2021 Blog. All rights reserved.
    </p>
</div>

</body>
</html>
